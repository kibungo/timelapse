/**
 * Copyright 2013 Nils Assbeck, Guersel Ayaz and Michael Zoech
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.remoteyourcam.usb.ptp;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.util.Log;

import com.remoteyourcam.usb.AppConfig;
import com.remoteyourcam.usb.ptp.Camera.CameraListener;
import com.remoteyourcam.usb.ptp.PtpCamera.State;

import java.util.Map;

import static android.hardware.usb.UsbManager.ACTION_USB_DEVICE_DETACHED;

public class PtpUsbService implements PtpService {

    private final String TAG = PtpUsbService.class.getSimpleName();

    private static final String ACTION_USB_PERMISSION = "com.remoteyourcam.usb.ptp.USB_PERMISSION";

    private final BroadcastReceiver permissionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (ACTION_USB_PERMISSION.equals(action)) {
                context.unregisterReceiver(permissionReceiver);

                synchronized (this) {
                    UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            connect(context, device);
                        } else {
                            listener.onError("Can't connect to device");
                        }
                    } else {
                        Log.d(TAG, "Permission denied for device " + device);
                        listener.onError("Permission denied for device");
                    }
                }
            }
        }
    };

    private final BroadcastReceiver deviceDetachedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (ACTION_USB_DEVICE_DETACHED.equals(action)) {
                if (AppConfig.LOG) {
                    Log.d(TAG, "Usb device detached");
                }

                context.unregisterReceiver(deviceDetachedReceiver);

                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                if (device == null || camera == null) {
                    return;
                }

                shutdownHard();

                if (listener != null) {
                    listener.onCameraStopped(camera);
                }
            }
        }

    };

    private final UsbManager usbManager;
    private PtpCamera camera;
    private CameraListener listener;

    public PtpUsbService(Context context) {
        this.usbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
    }

    @Override
    public void setCameraListener(CameraListener listener) {
        this.listener = listener;
        if (camera != null) {
            camera.setListener(listener);
        }
    }

    @Override
    public void initialize(Context context, Intent intent) {
        if (camera != null) {
            if (AppConfig.LOG) {
                Log.i(TAG, "initialize: camera available");
            }
            if (camera.getState() == State.Active) {
                if (listener != null) {
                    listener.onCameraStarted(camera);
                }
                return;
            }
            if (AppConfig.LOG) {
                Log.i(TAG, "initialize: camera not active, state " + camera.getState());
            }

            shutdownHard();
        }

        UsbDevice device = (intent != null) ? (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE) : null;
        if (device != null && usbManager.hasPermission(device)) {
            if (AppConfig.LOG) {
                Log.i(TAG, "initialize: got device through intent");
            }
            connect(context, device);
        } else {
            if (AppConfig.LOG) {
                Log.i(TAG, "initialize: looking for compatible camera");
            }
            device = lookupCompatibleDevice(usbManager);
            if (device != null) {
                registerPermissionReceiver(context);
                PendingIntent mPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(
                        ACTION_USB_PERMISSION), 0);
                usbManager.requestPermission(device, mPermissionIntent);
            } else {
                listener.onNoCameraFound();
            }
        }
    }

    @Override
    public void shutdown() {
        if (AppConfig.LOG) {
            Log.i(TAG, "shutdown");
        }

        if (camera != null) {
            camera.shutdown();
            camera = null;
        }
    }

    private void shutdownHard() {
        if (AppConfig.LOG) {
            Log.i(TAG, "shutdown hard");
        }

        if (camera != null) {
            camera.shutdownHard();
            camera = null;
        }
    }

    private void registerPermissionReceiver(Context context) {
        if (AppConfig.LOG) {
            Log.i(TAG, "register permission receiver");
        }

        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        context.registerReceiver(permissionReceiver, filter);
    }

    private void registerDeviceDetachReceiver(Context context) {
        IntentFilter filter = new IntentFilter(ACTION_USB_DEVICE_DETACHED);
        context.registerReceiver(deviceDetachedReceiver, filter);
    }

    private UsbDevice lookupCompatibleDevice(UsbManager manager) {
        Map<String, UsbDevice> deviceList = manager.getDeviceList();
        for (Map.Entry<String, UsbDevice> e : deviceList.entrySet()) {
            UsbDevice d = e.getValue();
            if (d.getVendorId() == PtpConstants.CanonVendorId || d.getVendorId() == PtpConstants.NikonVendorId) {
                return d;
            }
        }
        return null;
    }

    private boolean connect(Context context, UsbDevice device) {
        if (camera != null) {
            camera.shutdown();
            camera = null;
        }

        if (device.getVendorId() != PtpConstants.CanonVendorId && device.getVendorId() != PtpConstants.NikonVendorId) {
            if (listener != null) {
                listener.onError("No compatible camera found");
            }

            return false;
        }

        for (int i = 0, n = device.getInterfaceCount(); i < n; ++i) {
            UsbInterface intf = device.getInterface(i);

            if (intf.getEndpointCount() != 3) {
                continue;
            }

            UsbEndpoint in = null;
            UsbEndpoint out = null;

            for (int e = 0, en = intf.getEndpointCount(); e < en; ++e) {
                UsbEndpoint endpoint = intf.getEndpoint(e);
                if (endpoint.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                    if (endpoint.getDirection() == UsbConstants.USB_DIR_IN) {
                        in = endpoint;
                    } else if (endpoint.getDirection() == UsbConstants.USB_DIR_OUT) {
                        out = endpoint;
                    }
                }
            }

            if (in == null || out == null) {
                continue;
            }

            if (AppConfig.LOG) {
                Log.i(TAG, "Found compatible USB interface");
                Log.i(TAG, "Interface class " + intf.getInterfaceClass());
                Log.i(TAG, "Interface subclass " + intf.getInterfaceSubclass());
                Log.i(TAG, "Interface protocol " + intf.getInterfaceProtocol());
                Log.i(TAG, "Bulk out max size " + out.getMaxPacketSize());
                Log.i(TAG, "Bulk in max size " + in.getMaxPacketSize());
            }

            UsbDeviceConnection usbDeviceConnection = usbManager.openDevice(device);
            usbDeviceConnection.claimInterface(intf, true);

            PtpUsbConnection connection = new PtpUsbConnection(usbDeviceConnection, intf, in, out,
                    device.getVendorId(), device.getProductId());

            if (device.getVendorId() == PtpConstants.CanonVendorId) {
                camera = new EosCamera(connection, listener, new WorkerNotifier(context));
            } else if (device.getVendorId() == PtpConstants.NikonVendorId) {
                camera = new NikonCamera(connection, listener, new WorkerNotifier(context));
            }

            registerDeviceDetachReceiver(context);

            return true;
        }

        if (listener != null) {
            listener.onError("No compatible camera found");
        }

        return false;
    }
}
