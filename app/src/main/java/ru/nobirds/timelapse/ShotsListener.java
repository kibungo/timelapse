package ru.nobirds.timelapse;

public interface ShotsListener {

    void onNewShot();

}
