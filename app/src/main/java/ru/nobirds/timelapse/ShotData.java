package ru.nobirds.timelapse;

import android.graphics.Bitmap;

public class ShotData {

    private final int shotNumber;
    private final int shotsRequestedCount;

    private final String filename;
    private final Bitmap bitmap;

    private final boolean error;
    private final String errorMessage;

    public ShotData(int shotNumber, int shotsRequestedCount, String filename, Bitmap bitmap) {
        this.shotNumber = shotNumber;
        this.shotsRequestedCount = shotsRequestedCount;

        this.filename = filename;
        this.bitmap = bitmap;

        this.error = false;
        this.errorMessage = null;
    }

    public ShotData(ShotData prev, String errorMessage) {
        this.shotNumber = prev.shotNumber;
        this.shotsRequestedCount = prev.shotsRequestedCount;

        this.filename = prev.filename;
        this.bitmap = prev.bitmap;

        this.error = true;
        this.errorMessage = errorMessage;
    }

    public int getShotNumber() {
        return shotNumber;
    }

    public int getShotsRequestedCount() {
        return shotsRequestedCount;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getFilename() {
        return filename;
    }

    public boolean isFinal() {
        return shotNumber == shotsRequestedCount;
    }

    public boolean isError() {
        return error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

}
