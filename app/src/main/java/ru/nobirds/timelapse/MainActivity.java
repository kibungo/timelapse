package ru.nobirds.timelapse;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import ru.nobirds.timelapse.camera.CameraValue;
import ru.nobirds.timelapse.camera.MyCamera;
import ru.nobirds.timelapse.camera.MyCameraListener;
import ru.nobirds.timelapse.dialogs.ErrorDialogFragment;

public class MainActivity extends AppCompatActivity implements MyCameraListener, ShotsListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    public static final String ERROR_DIALOG_FRAGMENT_TAG = "ErrorDialogFragment";

    private TextView statusTextView;
    private Spinner expositionSpinner;
    private Spinner isoSpinner;
    private Spinner apertureSpinner;
    private Button applyParametersButton;
    private EditText shotsCountEditText;
    private EditText shotsIntervalEditText;
    private TextView totalTimeTextView;
    private Button captureButton;
    private TextView progressTextView;
    private ImageView captureImageView;

    private SparseArray<Integer> expositionValuesToPositionMap = new SparseArray<>();
    private SparseArray<Integer> isoValuesToPositionMap = new SparseArray<>();
    private SparseArray<Integer> apertureValuesToPositionMap = new SparseArray<>();

    private boolean isInStart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        statusTextView = (TextView) findViewById(R.id.statusTextView);
        expositionSpinner = (Spinner) findViewById(R.id.expositionSpinner);
        isoSpinner = (Spinner) findViewById(R.id.isoSpinner);
        apertureSpinner = (Spinner) findViewById(R.id.apertureSpinner);
        applyParametersButton = (Button) findViewById(R.id.applyParametersButton);
        shotsCountEditText = (EditText) findViewById(R.id.shotsCountEditText);
        shotsIntervalEditText = (EditText) findViewById(R.id.shotsIntervalEditText);
        totalTimeTextView = (TextView) findViewById(R.id.totalTimeTextView);
        progressTextView = (TextView) findViewById(R.id.progressTextView);
        captureImageView = (ImageView) findViewById(R.id.captureImageView);
        captureButton = (Button) findViewById(R.id.captureButton);

        if (savedInstanceState == null) {
            shotsCountEditText.setText(R.string.default_shots_count);
            shotsIntervalEditText.setText(R.string.default_interval);
            calculateTotalTime();
        }

        TextWatcher totalTimeTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // DO NOTHING
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // DO NOTHING
            }

            @Override
            public void afterTextChanged(Editable s) {
                calculateTotalTime();
            }
        };

        shotsCountEditText.addTextChangedListener(totalTimeTextWatcher);
        shotsIntervalEditText.addTextChangedListener(totalTimeTextWatcher);
    }

    @Override
    protected void onStart() {
        super.onStart();

        isInStart = true;

        ShotData shot = TimelapseService.getLastShot();

        showLastShot(shot);

        if (shot != null && shot.isError()) {
            onError(shot.getErrorMessage());
            return;
        }

        MyCamera camera = MyCamera.getInstance();
        camera.addListener(this);
        camera.incUsersCount();
        initCamera();

        TimelapseService.setShotsListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        isInStart = false;

        MyCamera camera = MyCamera.getInstance();
        camera.removeListener(this);
        camera.decUsersCount();

        TimelapseService.setShotsListener(null);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        Log.d(TAG, "New intent " + intent.getAction());

        setIntent(intent);

        if (isInStart) {
            initCamera();
        }
    }

    private void initCamera() {
        setStatusText(R.string.status_no_camera);
        MyCamera.getInstance().initialize(getApplicationContext(), getIntent());
    }

    @Override
    public void onCameraStarted() {
        Log.d(TAG, "onCameraStarted");

        setStatusText(R.string.status_camera_connected);

        initParameters();

        if (TimelapseService.isTimelapseInProcess()) {
            showStopTimelapseButton();
        } else {
            showStartTimelapseButton(true);
        }

        setParametersEnabled(true);
    }

    @Override
    public void onCameraStopped() {
        Log.d(TAG, "onCameraStopped");

        showStartTimelapseButton(false);
        setParametersEnabled(false);
        setStatusText(R.string.status_no_camera);

        if (TimelapseService.isTimelapseInProcess()) {
            onError("Camera has been stopped");
        }
    }

    @Override
    public void onNoCameraFound() {
        Log.d(TAG, "onNoCameraFound");

        showStartTimelapseButton(false);
        setParametersEnabled(false);
        setStatusText(R.string.status_no_camera);

        if (TimelapseService.isTimelapseInProcess()) {
            onError("No camera found");
        }
    }

    @Override
    public void onError(String message) {
        Log.d(TAG, "onError");

        showStartTimelapseButton(false);
        setParametersEnabled(false);

        if (TimelapseService.isTimelapseInProcess()) {
            setStatusText(R.string.status_error);
            showErrorDialog(message);
        } else {
            setStatusText(R.string.status_no_camera);
        }
    }

    private void showErrorDialog(String message) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        DialogFragment dialogFragment = (DialogFragment) fragmentManager.findFragmentByTag(ERROR_DIALOG_FRAGMENT_TAG);

        if (dialogFragment == null || dialogFragment.getDialog() == null || !dialogFragment.getDialog().isShowing()) {
            ErrorDialogFragment.newInstance(message).show(fragmentManager, ERROR_DIALOG_FRAGMENT_TAG);
        }
    }

    @Override
    public void onCapturedPictureReceived(String filename, Bitmap bitmap) {
        // DO NOTHING: we use ShotsListener.onNewShot() to receive updates
    }

    @Override
    public void onNewShot() {
        Log.d(TAG, "onNewShot");

        ShotData lastShot = TimelapseService.getLastShot();

        showLastShot(lastShot);

        if (lastShot.isFinal()) {
            showStartTimelapseButton(true);
        }
    }

    private void initParameters() {
        MyCamera camera = MyCamera.getInstance();

        onExpositionPossibleValuesChanged(camera.getExpositionValues());
        onExpositionValueChanged(camera.getExposition());

        onIsoPossibleValuesChanged(camera.getIsoValues());
        onIsoValueChanged(camera.getIso());

        onAperturePossibleValuesChanged(camera.getApertureValues());
        onApertureValueChanged(camera.getAperture());
    }

    @Override
    public void onExpositionPossibleValuesChanged(List<CameraValue> possibleValues) {
        Collections.reverse(possibleValues); // print expositions from longest to shortest

        indexPossibleValues(expositionValuesToPositionMap, possibleValues);
        expositionSpinner.setAdapter(createSpinnerAdapter(possibleValues));
    }

    @Override
    public void onExpositionValueChanged(int value) {
        Integer position = expositionValuesToPositionMap.get(value);

        if (position != null) {
            expositionSpinner.setSelection(position);
        } else {
            Log.e(TAG, "No spinner item for exposition value");
        }
    }

    @Override
    public void onIsoPossibleValuesChanged(List<CameraValue> possibleValues) {
        indexPossibleValues(isoValuesToPositionMap, possibleValues);
        isoSpinner.setAdapter(createSpinnerAdapter(possibleValues));
    }

    @Override
    public void onIsoValueChanged(int value) {
        Integer position = isoValuesToPositionMap.get(value);

        if (position != null) {
            isoSpinner.setSelection(position);
        } else {
            Log.e(TAG, "No spinner item for ISO value");
        }
    }

    @Override
    public void onAperturePossibleValuesChanged(List<CameraValue> possibleValues) {
        indexPossibleValues(apertureValuesToPositionMap, possibleValues);
        apertureSpinner.setAdapter(createSpinnerAdapter(possibleValues));
    }

    private ArrayAdapter<CameraValue> createSpinnerAdapter(List<CameraValue> possibleValues) {
        return new ArrayAdapter<>(this, R.layout.spinner_item, possibleValues);
    }

    @Override
    public void onApertureValueChanged(int value) {
        Integer position = apertureValuesToPositionMap.get(value);

        if (position != null) {
            apertureSpinner.setSelection(position);
        } else {
            Log.e(TAG, "No spinner item for aperture value");
        }
    }

    private void indexPossibleValues(SparseArray<Integer> valuesToPositionMap, List<CameraValue> possibleValues) {
        valuesToPositionMap.clear();

        int i = 0;
        for (CameraValue cameraValue : possibleValues) {
            valuesToPositionMap.put(cameraValue.getValue(), i++);
        }
    }

    public void onApplyParametersButtonClick(View view) {
        CameraValue selectedExpositionItem = (CameraValue) expositionSpinner.getSelectedItem();

        if (selectedExpositionItem != null) {
            MyCamera.getInstance().setExposition(selectedExpositionItem.getValue());
        } else {
            Log.e(TAG, "Selected exposition is null");
        }

        // ---

        CameraValue selectedIsoItem = (CameraValue) isoSpinner.getSelectedItem();

        if (selectedIsoItem != null) {
            MyCamera.getInstance().setIso(selectedIsoItem.getValue());
        } else {
            Log.e(TAG, "Selected ISO is null");
        }

        // ---

        CameraValue selectedApertureItem = (CameraValue) apertureSpinner.getSelectedItem();

        if (selectedApertureItem != null) {
            MyCamera.getInstance().setAperture(selectedApertureItem.getValue());
        } else {
            Log.e(TAG, "Selected aperture is null");
        }
    }

    public void reset() {
        showStartTimelapseButton(false);
        setParametersEnabled(false);
        showLastShot(null);

        TimelapseService.clearLastShot();
        initCamera();
    }

    private void showLastShot(ShotData shot) {
        if (shot == null) {
            captureImageView.setImageResource(R.drawable.camera_grey);
            captureImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            progressTextView.setVisibility(View.INVISIBLE);
            return;
        }

        if (shot.getBitmap() != null) {
            captureImageView.setImageBitmap(shot.getBitmap());
            captureImageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
        } else {
            captureImageView.setImageResource(R.drawable.camera_grey);
            captureImageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
        }

        progressTextView.setText(getString(R.string.progress_text, shot.getShotNumber(), shot.getShotsRequestedCount()));
        progressTextView.setVisibility(View.VISIBLE);
    }

    private void setStatusText(int resId) {
        statusTextView.setText(getString(R.string.status_text, getString(resId)));
    }

    private void calculateTotalTime() {
        String countStr = shotsCountEditText.getText().toString();
        String intervalStr = shotsIntervalEditText.getText().toString();

        if (!Utils.isPositiveInteger(countStr) || !Utils.isPositiveInteger(intervalStr)) {
            totalTimeTextView.setVisibility(View.INVISIBLE);
            return;
        }

        int count = Integer.parseInt(countStr);
        int interval = Integer.parseInt(intervalStr);

        if (count > 0) {
            --count; // считаем количество интервалов между кадрами, а не количество кадров
        }

        long ms = count * interval * 1000;
        long hr = TimeUnit.MILLISECONDS.toHours(ms);
        long min = TimeUnit.MILLISECONDS.toMinutes(ms - TimeUnit.HOURS.toMillis(hr));

        totalTimeTextView.setText(getString(R.string.total_time, String.format(Locale.US, "%02dh %02dm", hr, min)));
        totalTimeTextView.setVisibility(View.VISIBLE);
    }

    private void showStartTimelapseButton(boolean enabled) {
        captureButton.setText(R.string.start_timelapse_button_text);
        captureButton.setEnabled(enabled);

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String countStr = shotsCountEditText.getText().toString();
                String intervalStr = shotsIntervalEditText.getText().toString();

                if (!Utils.isPositiveInteger(countStr) || !Utils.isPositiveInteger(intervalStr)) {
                    showCountIntervalFormatMessage();
                    return;
                }

                int count = Integer.parseInt(countStr);
                long interval = 1000L * Integer.parseInt(intervalStr);

                if (count <= 0 || interval <= 0) {
                    showCountIntervalFormatMessage();
                    return;
                }

                TimelapseService.startTimelapse(MainActivity.this, interval, count);
                showStopTimelapseButton();
            }
        });
    }

    private void showStopTimelapseButton() {
        captureButton.setText(R.string.stop_timelapse_button_text);
        captureButton.setEnabled(true);

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                captureButton.setEnabled(false);
                TimelapseService.stopTimelapse(MainActivity.this);
            }
        });
    }

    private void showCountIntervalFormatMessage() {
        Toast.makeText(this, "Count and interval must be whole numbers greater then 0", Toast.LENGTH_SHORT).show();
    }

    private void setParametersEnabled(boolean enabled) {
        applyParametersButton.setEnabled(enabled);
        expositionSpinner.setEnabled(enabled);
        isoSpinner.setEnabled(enabled);
        apertureSpinner.setEnabled(enabled);
    }

}
