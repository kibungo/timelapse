package ru.nobirds.timelapse.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ru.nobirds.timelapse.MainActivity;
import ru.nobirds.timelapse.R;

public class ErrorDialogFragment extends DialogFragment {

    private static final String MESSAGE_TAG = "message";

    public static ErrorDialogFragment newInstance(String message) {
        ErrorDialogFragment result = new ErrorDialogFragment();

        Bundle args = new Bundle();
        args.putString(MESSAGE_TAG, message);
        result.setArguments(args);

        return result;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(R.string.error_dialog_title)
                .setMessage(getArguments().getString(MESSAGE_TAG))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Activity activity = getActivity();

                        if (activity instanceof MainActivity) {
                            ((MainActivity) activity).reset();
                        }
                    }
                });

        setCancelable(false);

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

}
