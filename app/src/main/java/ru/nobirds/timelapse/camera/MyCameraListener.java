package ru.nobirds.timelapse.camera;

import android.graphics.Bitmap;

import java.util.List;

public interface MyCameraListener {

    void onCameraStarted();

    void onCameraStopped();

    void onNoCameraFound();

    void onError(String message);

    void onCapturedPictureReceived(String filename, Bitmap bitmap);

    void onExpositionPossibleValuesChanged(List<CameraValue> possibleValues);

    void onExpositionValueChanged(int value);

    void onIsoPossibleValuesChanged(List<CameraValue> possibleValues);

    void onIsoValueChanged(int value);

    void onAperturePossibleValuesChanged(List<CameraValue> possibleValues);

    void onApertureValueChanged(int value);

}
