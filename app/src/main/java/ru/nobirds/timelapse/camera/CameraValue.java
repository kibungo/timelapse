package ru.nobirds.timelapse.camera;

public class CameraValue {

    private final int value;
    private final String text;

    public CameraValue(int value, String text) {
        if (text == null || text.isEmpty()) {
            throw new IllegalArgumentException();
        }

        this.value = value;
        this.text = text;
    }

    public int getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return text;
    }

}
