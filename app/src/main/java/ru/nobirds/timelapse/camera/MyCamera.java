package ru.nobirds.timelapse.camera;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

import com.remoteyourcam.usb.ptp.Camera;
import com.remoteyourcam.usb.ptp.PtpService;
import com.remoteyourcam.usb.ptp.model.LiveViewData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MyCamera implements Camera.CameraListener {

    private static final String TAG = MyCamera.class.getSimpleName();

    private PtpService ptpService = null;
    private Camera camera = null;

    private Set<MyCameraListener> listeners = new HashSet<>();

    private int usersCounter = 0;

    private final static MyCamera singleton = new MyCamera();

    public static MyCamera getInstance() {
        return singleton;
    }

    public void initialize(Context context, Intent intent) {
        if (ptpService == null) {
            ptpService = PtpService.Singleton.getInstance(context);
            ptpService.setCameraListener(this);
        }

        camera = null;
        ptpService.initialize(context, intent);
    }

    public boolean isConnected() {
        return camera != null && camera.isSessionOpen();
    }

    public void incUsersCount() {
        usersCounter++;
    }

    public void decUsersCount() {
        if (usersCounter > 0) {
            usersCounter--;

            if (usersCounter == 0) {
                shutdown();
            }
        }
    }

    private void shutdown() {
        Log.d(TAG, "MyCamera shutdown");
        camera = null;
        ptpService.shutdown();
    }

    public void addListener(MyCameraListener listener) {
        listeners.add(listener);
    }

    public void removeListener(MyCameraListener listener) {
        listeners.remove(listener);
    }

    public void capture() {
        if (!isConnected()) {
            onError("Camera is not active");
            return;
        }

        camera.capture();
    }

    public List<CameraValue> getExpositionValues() {
        return getPossibleValues(Camera.Property.ShutterSpeed);
    }

    public List<CameraValue> getIsoValues() {
        return getPossibleValues(Camera.Property.IsoSpeed);
    }

    public List<CameraValue> getApertureValues() {
        return getPossibleValues(Camera.Property.ApertureValue);
    }

    private List<CameraValue> getPossibleValues(int property) {
        if (!isConnected()) {
            onError("Camera is not active");
            return Collections.emptyList();
        }

        return createPossibleValues(property, camera.getPropertyDesc(property));
    }

    private List<CameraValue> createPossibleValues(int property, int[] propertyDesc) {
        List<CameraValue> result = new ArrayList<>(propertyDesc.length);

        for (int value : propertyDesc) {
            String text = camera.propertyToString(property, value);
            result.add(new CameraValue(value, text));
        }

        return result;
    }

    public int getExposition() {
        return getProperty(Camera.Property.ShutterSpeed);
    }

    public void setExposition(int value) {
        setProperty(Camera.Property.ShutterSpeed, value);
    }

    public int getIso() {
        return getProperty(Camera.Property.IsoSpeed);
    }

    public void setIso(int value) {
        setProperty(Camera.Property.IsoSpeed, value);
    }

    public int getAperture() {
        return getProperty(Camera.Property.ApertureValue);
    }

    public void setAperture(int value) {
        setProperty(Camera.Property.ApertureValue, value);
    }

    private int getProperty(int property) {
        if (!isConnected()) {
            onError("Camera is not active");
            return 0;
        }

        return camera.getProperty(property);
    }

    private void setProperty(int property, int value) {
        if (!isConnected()) {
            onError("Camera is not active");
            return;
        }

        camera.setProperty(property, value);
    }

    public void riseError(String message) {
        Log.d(TAG, "Rising error: " + message);
        onError(message);
    }

    @Override
    public void onCameraStarted(Camera camera) {
        Log.d(TAG, "Camera started");

        this.camera = camera;

        for (MyCameraListener listener : listeners) {
            listener.onCameraStarted();
        }
    }

    @Override
    public void onCameraStopped(Camera camera) {
        Log.d(TAG, "Camera stopped");

        this.camera = null;

        for (MyCameraListener listener : listeners) {
            listener.onCameraStopped();
        }
    }

    @Override
    public void onNoCameraFound() {
        Log.d(TAG, "onNoCameraFound");

        for (MyCameraListener listener : listeners) {
            listener.onNoCameraFound();
        }
    }

    @Override
    public void onError(String message) {
        Log.d(TAG, "onError");

        for (MyCameraListener listener : listeners) {
            listener.onError(message);
        }
    }

    @Override
    public void onObjectAdded(int handle, int format) {
        if (!isConnected()) {
            onError("Camera is not active");
            return;
        }

        camera.retrievePicture(handle);
//        onCapturedPictureReceived(handle, "bla-bla", null, null); // to run without picture retrieval
    }

    @Override
    public void onCapturedPictureReceived(int objectHandle, String filename, Bitmap thumbnail, Bitmap bitmap) {
        for (MyCameraListener listener : listeners) {
            listener.onCapturedPictureReceived(filename, bitmap);
        }
    }

    @Override
    public void onPropertyDescChanged(int property, int[] propertyDesc) {
        if (property == Camera.Property.ShutterSpeed) {
            for (MyCameraListener listener : listeners) {
                listener.onExpositionPossibleValuesChanged(createPossibleValues(property, propertyDesc));
            }
        }

        if (property == Camera.Property.IsoSpeed) {
            for (MyCameraListener listener : listeners) {
                listener.onIsoPossibleValuesChanged(createPossibleValues(property, propertyDesc));
            }
        }

        if (property == Camera.Property.ApertureValue) {
            for (MyCameraListener listener : listeners) {
                listener.onAperturePossibleValuesChanged(createPossibleValues(property, propertyDesc));
            }
        }
    }

    @Override
    public void onPropertyChanged(int property, int value) {
        if (property == Camera.Property.ShutterSpeed) {
            for (MyCameraListener listener : listeners) {
                listener.onExpositionValueChanged(value);
            }
        }

        if (property == Camera.Property.IsoSpeed) {
            for (MyCameraListener listener : listeners) {
                listener.onIsoValueChanged(value);
            }
        }

        if (property == Camera.Property.ApertureValue) {
            for (MyCameraListener listener : listeners) {
                listener.onApertureValueChanged(value);
            }
        }
    }

    @Override
    public void onPropertyStateChanged(int property, boolean enabled) {
        // DO NOTHING
    }

    @Override
    public void onLiveViewStarted() {
        // DO NOTHING
    }

    @Override
    public void onLiveViewData(LiveViewData data) {
        // DO NOTHING
    }

    @Override
    public void onLiveViewStopped() {
        // DO NOTHING
    }

    @Override
    public void onBulbStarted() {
        // DO NOTHING
    }

    @Override
    public void onBulbExposureTime(int seconds) {
        // DO NOTHING
    }

    @Override
    public void onBulbStopped() {
        // DO NOTHING
    }

    @Override
    public void onFocusStarted() {
        // DO NOTHING
    }

    @Override
    public void onFocusEnded(boolean hasFocused) {
        // DO NOTHING
    }

    @Override
    public void onFocusPointsChanged() {
        // DO NOTHING
    }

}
