package ru.nobirds.timelapse;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.util.Log;

import java.util.List;
import java.util.Locale;

import ru.nobirds.timelapse.camera.CameraValue;
import ru.nobirds.timelapse.camera.MyCamera;
import ru.nobirds.timelapse.camera.MyCameraListener;

public class TimelapseService extends Service implements MyCameraListener {

    private static final String TAG = TimelapseService.class.getSimpleName();

    public static final String SHOTS_COUNT_TAG = "SHOTS_COUNT";
    public static final String CAPTURE_INTERVAL_TAG = "CAPTURE_INTERVAL";

    private static final int FOREGROUND_NOTIFICATION_ID = 9001;
    private static final int RESULT_NOTIFICATION_ID = 9002;

    private class LifeCheckRunnable implements Runnable {

        private final int shotsCount;

        public LifeCheckRunnable(int shotsCount) {
            this.shotsCount = shotsCount;
        }

        @Override
        public void run() {
            if (!isTimelapseInProcess()) {
                Log.d(TAG, "Timelapse is not in process");
                return;
            }

            if (shotsReceivedCount < shotsCount) {
                MyCamera.getInstance().riseError("Camera is not responding");
            }
        }

    }

    private final Runnable captureRunnable = new Runnable() {

        @Override
        public void run() {
            if (!isTimelapseInProcess()) {
                Log.d(TAG, "Timelapse is not in process");
                return;
            }

            Log.d(TAG, "Capture");

            MyCamera.getInstance().capture();
            shotsMadeCount++;

            handler.postDelayed(new LifeCheckRunnable(shotsMadeCount), 60 * 1000 + captureInterval);

            if (shotsMadeCount < shotsRequestedCount) {
                handler.postDelayed(this, captureInterval);
            } else {
                Log.d(TAG, "Finished capturing");
            }
        }

    };

    private final Handler handler = new Handler();

    private PowerManager.WakeLock wakeLock = null;

    private long captureInterval;

    private int shotsRequestedCount;
    private int shotsMadeCount;
    private int shotsReceivedCount;

    private static ShotsListener shotsListener;
    private static ShotData lastShot = null;

    public TimelapseService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "Service start");

        handler.removeCallbacksAndMessages(null);

        captureInterval = intent.getLongExtra(CAPTURE_INTERVAL_TAG, 0);
        shotsRequestedCount = intent.getIntExtra(SHOTS_COUNT_TAG, 0);

        if (captureInterval <= 0 || shotsRequestedCount <= 0) {
            stopSelf();
            return START_NOT_STICKY;
        }

        shotsMadeCount = 0;
        shotsReceivedCount = 0;

        startForeground(FOREGROUND_NOTIFICATION_ID, createForegroundNotification());
        acquireWakeLock();

        MyCamera.getInstance().addListener(this);
        captureRunnable.run();

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "Service onDestroy");

        releaseWakeLock();

        handler.removeCallbacksAndMessages(null);

        MyCamera.getInstance().removeListener(this);
        MyCamera.getInstance().decUsersCount();
    }

    private void acquireWakeLock() {
        if (wakeLock == null) {
            PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
            PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakelockTag");
            wakeLock.acquire();
        }
    }

    private void releaseWakeLock() {
        if (wakeLock != null) {
            wakeLock.release();
            wakeLock = null;
        }
    }

    @Override
    public void onCameraStarted() {
        // DO NOTHING
    }

    @Override
    public void onCameraStopped() {
        if (shotsReceivedCount < shotsRequestedCount) {
            onError("Camera has been stopped");
        }
    }

    @Override
    public void onNoCameraFound() {
        if (shotsReceivedCount < shotsRequestedCount) {
            onError("No camera found");
        }
    }

    @Override
    public void onError(String message) {
        Log.d(TAG, "Service onError");

        lastShot = new ShotData(lastShot, message);

        showNotification(RESULT_NOTIFICATION_ID, createErrorNotification(message));

        stopSelf();
    }

    @Override
    public void onCapturedPictureReceived(String filename, Bitmap bitmap) {
        Log.d(TAG, "Service: onCapturedPictureReceived");

        if (!isTimelapseInProcess()) {
            Log.d(TAG, "Timelapse is not in process");
            return;
        }

        shotsReceivedCount++;

        setLastShot(
                new ShotData(
                        shotsReceivedCount,
                        shotsRequestedCount,
                        filename,
                        bitmap
                )
        );

        if (shotsReceivedCount < shotsRequestedCount) {
            updateForegroundNotification();
        } else {
            Log.d(TAG, "Last picture received. Stopping service.");
            showNotification(RESULT_NOTIFICATION_ID, createResultNotification());
            stopSelf();
        }
    }

    @Override
    public void onExpositionPossibleValuesChanged(List<CameraValue> possibleValues) {
        // DO NOTHING
    }

    @Override
    public void onExpositionValueChanged(int value) {
        // DO NOTHING
    }

    @Override
    public void onIsoPossibleValuesChanged(List<CameraValue> possibleValues) {
        // DO NOTHING
    }

    @Override
    public void onIsoValueChanged(int value) {
        // DO NOTHING
    }

    @Override
    public void onAperturePossibleValuesChanged(List<CameraValue> possibleValues) {
        // DO NOTHING
    }

    @Override
    public void onApertureValueChanged(int value) {
        // DO NOTHING
    }

    private Notification createForegroundNotification() {
        String progressStr = String.format(Locale.US, "%d/%d", shotsReceivedCount, shotsRequestedCount);

        Notification.Builder builder = new Notification.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon))
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("Timelapse")
                .setTicker("Timelapse progress " + progressStr)
                .setProgress(shotsRequestedCount, shotsReceivedCount, false)
                .setContentText("Progress: " + progressStr)
                .setOngoing(true)
                .setContentIntent(createNotificationIntent())
                .setContentIntent(createNotificationIntent());

        return builder.build();
    }

    private Notification createErrorNotification(String message) {
        Notification.Builder builder = new Notification.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon))
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("Timelapse error")
                .setTicker("Timelapse error")
                .setContentText(message)
                .setContentIntent(createNotificationIntent());

        return builder.build();
    }

    private Notification createResultNotification() {
        Notification.Builder builder = new Notification.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.icon))
                .setSmallIcon(R.drawable.icon)
                .setContentTitle("Timelapse is ready")
                .setTicker("Timelapse is ready")
                .setContentIntent(createNotificationIntent());

        return builder.build();
    }

    private PendingIntent createNotificationIntent() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        return PendingIntent.getActivity(this, 0, intent, 0);
    }

    private void updateForegroundNotification() {
        showNotification(FOREGROUND_NOTIFICATION_ID, createForegroundNotification());
    }

    private void showNotification(int id, Notification notification) {
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(id, notification);
    }

    private static void closeNotification(Context context, int id) {
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(id);
    }

    public static void setShotsListener(ShotsListener shotsListener) {
        TimelapseService.shotsListener = shotsListener;
    }

    private static void setLastShot(ShotData shot) {
        lastShot = shot;

        if (shotsListener != null) {
            shotsListener.onNewShot();
        }
    }

    public static void clearLastShot() {
        lastShot = null;
    }

    public static ShotData getLastShot() {
        return lastShot;
    }

    public static boolean isTimelapseInProcess() {
        return lastShot != null && !lastShot.isFinal();
    }

    public static void startTimelapse(Context context, long interval, int shotsCount) {
        if (interval <= 0 || shotsCount <= 0) {
            throw new IllegalArgumentException();
        }

        if (isTimelapseInProcess()) {
            throw new IllegalStateException();
        }

        setLastShot(new ShotData(0, shotsCount, null, null));

        closeNotification(context, RESULT_NOTIFICATION_ID);

        Intent intent = new Intent(context, TimelapseService.class);
        intent.putExtra(TimelapseService.CAPTURE_INTERVAL_TAG, interval);
        intent.putExtra(TimelapseService.SHOTS_COUNT_TAG, shotsCount);
        context.startService(intent);

        MyCamera.getInstance().incUsersCount();
    }

    public static void stopTimelapse(Context context) {
        context.stopService(new Intent(context, TimelapseService.class));

        if (lastShot != null) {
            setLastShot(
                    new ShotData(
                            lastShot.getShotNumber(),
                            lastShot.getShotNumber(),
                            lastShot.getFilename(),
                            lastShot.getBitmap()
                    )
            );
        } else {
            setLastShot(new ShotData(0, 0, null, null));
        }
    }

}
