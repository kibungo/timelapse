package ru.nobirds.timelapse;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UtilsTest {

    @Test
    public void isPositiveInteger() throws Exception {
        assertTrue(Utils.isPositiveInteger("1"));
        assertTrue(Utils.isPositiveInteger("123"));
        assertTrue(Utils.isPositiveInteger("9999"));
        assertTrue(Utils.isPositiveInteger("0"));
        assertTrue(Utils.isPositiveInteger("0000"));
        assertTrue(Utils.isPositiveInteger("0123"));
        assertTrue(Utils.isPositiveInteger("0000123"));

        assertFalse(Utils.isPositiveInteger(""));
        assertFalse(Utils.isPositiveInteger(" "));
        assertFalse(Utils.isPositiveInteger("   "));
        assertFalse(Utils.isPositiveInteger(" 123"));
        assertFalse(Utils.isPositiveInteger("123 "));
        assertFalse(Utils.isPositiveInteger(" 123 "));
        assertFalse(Utils.isPositiveInteger("b123"));
        assertFalse(Utils.isPositiveInteger("123c"));
        assertFalse(Utils.isPositiveInteger("12d34"));
        assertFalse(Utils.isPositiveInteger(".123"));
        assertFalse(Utils.isPositiveInteger("123."));
        assertFalse(Utils.isPositiveInteger("123.0"));
        assertFalse(Utils.isPositiveInteger("123.0123"));
        assertFalse(Utils.isPositiveInteger("3.5"));
        assertFalse(Utils.isPositiveInteger("123$45"));
        assertFalse(Utils.isPositiveInteger("12345%"));
        assertFalse(Utils.isPositiveInteger("*12345"));
        assertFalse(Utils.isPositiveInteger("-123"));
        assertFalse(Utils.isPositiveInteger("+123"));
    }

}